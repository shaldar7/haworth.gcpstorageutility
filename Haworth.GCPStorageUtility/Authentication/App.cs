﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using Google.Apis.Services;

using System;
using System.Configuration;
using System.Threading;

namespace Haworth.GCPStorageUtility
{
    public class App
    {
        //private static CustomLogger _logger = (CustomLogger)LogManager.GetCurrentClassLogger(typeof(CustomLogger));
        public static UserCredential userCredentials { get; set; }

        public void setCredentials()
        {
            try
            {
                string filename = AppDomain.CurrentDomain.BaseDirectory + "//Authentication";

                var localfileDatastore = new LocalFileDataStore(filename);

                //var uc = GoogleWebAuthorizationBroker.AuthorizeAsync(
                //   new ClientSecrets { ClientId = ConfigurationManager.AppSettings["clientId"], ClientSecret = ConfigurationManager.AppSettings["clientSecret"] },
                //   new[] { DfareportingService.Scope.Dfareporting, DfareportingService.Scope.Dfatrafficking },
                //   ConfigurationManager.AppSettings["apiUser"],
                //   CancellationToken.None,
                //   localfileDatastore).Result;
                var scopes = new[] { @"https://www.googleapis.com/auth/devstorage.full_control" };
                var uc = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        new ClientSecrets { ClientId = ConfigurationManager.AppSettings["clientId"], ClientSecret = ConfigurationManager.AppSettings["clientSecret"] },
                         scopes,
                        ConfigurationManager.AppSettings["apiUser"],
                        CancellationToken.None,
                        localfileDatastore).Result;

                App.userCredentials = uc;
            }
            catch (Exception ex)
            {
                //_logger.SetSourceFileName(this);
                //_logger.SetErrorNumber(ex.HResult);
                //_logger.Error(ex.StackTrace);
                throw;
            }
        }
    }
}