﻿using Google.Api.Gax;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Storage.v1;
using Google.Apis.Storage.v1.Data;
using Google.Cloud.Storage.V1;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Haworth.GCPStorageUtility
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var options = new Options();
            new Program().Run(options).Wait();

        }
        private async Task Run(Options options)
        {
            try
            {
                // Authentication by using GoogleCredential
                //Getting bucket list by Project-id
                //https://cloud.google.com/storage/docs/reference/libraries#client-libraries-install-csharp
                string credential_path = AppDomain.CurrentDomain.BaseDirectory + @"Authentication\HMM-GCP-StorageUtility-a7c3eea51257.json";
                GoogleCredential credential = null;
                using (var jsonStream = new FileStream(credential_path, FileMode.Open,
                    FileAccess.Read, FileShare.Read))
                {
                    credential = GoogleCredential.FromStream(jsonStream);
                }

                StorageClient storageClient = StorageClient.Create(credential);

                PagedEnumerable<Buckets, Bucket> ls1 = storageClient.ListBuckets("hmm-gcputility");

                List<Bucket> ls2 = ls1.ToList();


                /////////////////////////////////////////////////////////
                //Autheticating by setting service Account key json as Environment variable
                string credential_path1 = AppDomain.CurrentDomain.BaseDirectory + @"Authentication\HMM-GCP-StorageUtility-a7c3eea51257.json";

                System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", credential_path1);

                StorageClient storageClient1 = StorageClient.Create();

                PagedEnumerable<Buckets, Bucket> ls3 = storageClient.ListBuckets("hmm-gcputility");

                List<Bucket> ls4 = ls1.ToList();

                ////////////////////////////////////////////////////////
                //Autheticate by using OAuth - need OAuth consent
                string filename = AppDomain.CurrentDomain.BaseDirectory + "//Authentication";

                var localfileDatastore = new LocalFileDataStore(filename);

                var uc = GoogleWebAuthorizationBroker.AuthorizeAsync(
                   new ClientSecrets { ClientId = ConfigurationManager.AppSettings["clientId"], ClientSecret = ConfigurationManager.AppSettings["clientSecret"] },
                   new[] { StorageService.Scope.CloudPlatform, StorageService.Scope.DevstorageReadWrite },
                   ConfigurationManager.AppSettings["apiUser"],
                   CancellationToken.None,
                   localfileDatastore).Result;

                StorageService service = new StorageService(new BaseClientService.Initializer
                {
                    HttpClientInitializer = uc,
                    ApplicationName = ConfigurationManager.AppSettings["ApplicationName"],
                });

                Google.Apis.Storage.v1.Data.Buckets ls = service.Buckets.List("hmm-gcputility").Execute();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
