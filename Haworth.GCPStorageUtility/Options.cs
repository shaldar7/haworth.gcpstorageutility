﻿using CommandLine;
using System;
using System.Collections.Generic;

namespace Haworth.GCPStorageUtility
{
    public class Options
    {
        [Option('n', "database_name", Required = true, HelpText = "Database name for Audit")]
        public string dbName { get; set; }

        [Option('v', "server_name", Required = true, HelpText = "Database name for Audit")]
        public string serverName { get; set; }

        [Option('s', "start_date", Required = false, HelpText = "Start Date for data extract")]
        public string startDate { get; set; }

        [Option('e', "end_date", Required = false, HelpText = "End Date for data extract")]
        public string endDate { get; set; }

        /// <summary>
        /// The function extracts timeRange.
        /// The method has return type of Dictionary<string, string></summary>
        public Dictionary<string, string> GetTimeRange()
        {
            var timeRange = new Dictionary<string, string>();

            string yesterday = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string lastWeek = DateTime.Now.AddDays(-8).ToString("yyyy-MM-dd");

            timeRange["since"] = startDate ?? lastWeek;
            timeRange["until"] = endDate ?? yesterday;

            return timeRange;
        }
    }
}